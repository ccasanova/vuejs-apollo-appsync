import VueApollo from 'vue-apollo'
import Vue from 'vue';
import appSyncConfig from '@/config/appsync'

/**** APOLLO */
import { ApolloClient } from 'apollo-client'
import { HttpLink } from 'apollo-link-http'
import { setContext } from 'apollo-link-context'
import { InMemoryCache } from 'apollo-cache-inmemory'

/** WS */
import { split } from 'apollo-link'
import { WebSocketLink } from 'apollo-link-ws'
import { getMainDefinition } from 'apollo-utilities'

const _HOST = appSyncConfig.url.replace("https://","").replace("/graphql", "")
const _API_KEY = appSyncConfig.apiKey

const _HEADERS_WS = {
  "host": _HOST,
  "x-api-key": _API_KEY
}

const HTTP_URI = 'https://' + _HOST + '/graphql'
const WS_URI = appSyncConfig.url.replace("https","wss").replace("api", "realtime-api")
              + '?header=' + btoa(JSON.stringify(_HEADERS_WS))
              + '&payload=' + btoa(JSON.stringify({}))
              + '&transports=websocket'

const httpLink = new HttpLink({
  // You should use an absolute URL here
  uri: HTTP_URI
})
const authLink = setContext((_, { headers }) => {
  return {
    headers: {
      ...headers,
      'x-api-key': _API_KEY,
      'token': localStorage.getItem('token')
    }
  }
})

const wsLink = new WebSocketLink({
  uri: WS_URI,
  options: {
    reconnect: true,
    lazy: true
  }
})
const subscriptionMiddleware = {
  applyMiddleware: async (options, next) => {
    options.data = JSON.stringify({
      'query': options.query.loc.source.body,
      'variables': options.variables
    })
    options.extensions = {
      'authorization': _HEADERS_WS
    }
    next()
  }
}
wsLink.subscriptionClient.use([subscriptionMiddleware])

// using the ability to split links, you can send data to each link
// depending on what kind of operation is being sent
const link = split(
  // split based on operation type
  ({ query }) => {
    const definition = getMainDefinition(query)
    return definition.kind === 'OperationDefinition' &&
      definition.operation === 'subscription'
  },
  wsLink,
  authLink.concat(httpLink)
)

// Create the apollo client
const apolloClient = new ApolloClient({
  link: link,
  cache: new InMemoryCache()
})

// Install the vue plugin
Vue.use(VueApollo)

export const apolloProvider = new VueApollo({
  defaultClient: apolloClient
})
/**** FIN APOLLO */
