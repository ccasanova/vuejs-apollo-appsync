import gql from 'graphql-tag'

export const ALL_PRODUCT_FILES = gql`
query AllProductFiles ($sellerId: String!, $page: Int!) {
  getProductsCatalogFiles(sellerId: $sellerId, page: $page){
    id
    urlCsv
    urlXls
  }
}
`
export const SUBSCRIPTION_CREATE_FILE = gql`
subscription WaitForCreateFile ($sellerId: String!){
  createdProductCatalogFiles(sellerId:$sellerId){
    id
    urlCsv
    urlXls
  }
}
`

export const MUTATION_CREATE_FILE = gql`
mutation CreateFile ($sellerId: String!, $filters: FiltersProductCatalogSchema){
  createProductCatalogFiles(sellerId:$sellerId, filters: $filters){
    sellerId
    id
    urlCsv
    urlXls
  }
}
`
