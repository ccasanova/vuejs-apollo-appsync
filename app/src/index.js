
import './index.scss';
import Vue from 'vue';
import App from './App'
import router from './router'
import Amplify, * as AmplifyModules from 'aws-amplify';

import appSyncConfig from '@/config/appsync'
const config = {
  aws_appsync_graphqlEndpoint: appSyncConfig.url,
  aws_appsync_region: appSyncConfig.region,
  aws_appsync_authenticationType: appSyncConfig.authType,
  aws_appsync_apiKey: appSyncConfig.apiKey
};

Amplify.configure(config);

Amplify.configure({
  API: {
    graphql_headers: async () => ({
        'token': localStorage.getItem('token')
    })
  }
});

Vue.use(AmplifyModules);

Vue.config.productionTip = false

new Vue({
  el: '#root',
  router,
  template: '<App/>',
  components: {
    App
  }
});
