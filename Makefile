.DEFAULT_GOAL := help
.EXPORT_ALL_VARIABLES:

## GENERAL ##
OWNER 			= vuejs
SERVICE_NAME 	= appsync
USERNAME_LOCAL  ?= "$(shell whoami)"
UID_LOCAL  		?= "$(shell id -u)"
GID_LOCAL  		?= "$(shell id -g)"


## RESULT_VARS ##
TAG_NODE		= node
PROJECT_NAME	= ${OWNER}-${SERVICE_NAME}
IMAGE_NODE		= ${PROJECT_NAME}:${TAG_NODE}

build-image:
	@docker build \
	--build-arg UID_LOCAL=$(UID_LOCAL) \
	--build-arg GID_LOCAL=$(GID_LOCAL) \
	-f docker/Dockerfile \
	-t $(IMAGE_NODE) docker/

init:
	@docker run --rm -it \
	-u $(UID_LOCAL):$(GID_LOCAL) \
	-v $(PWD)/app:/app \
	$(IMAGE_NODE) \
	npm init

install:
	@docker run --rm -it \
	-u $(UID_LOCAL):$(GID_LOCAL) \
	-v $(PWD)/app:/app \
	$(IMAGE_NODE) \
	npm install -d

install-package:
	@docker run --rm -it \
	-u $(UID_LOCAL):$(GID_LOCAL) \
	-v $(PWD)/app:/app \
	$(IMAGE_NODE) \
	npm install $(PACKAGE) --save

build:
	@docker run --rm -it \
	-v $(PWD)/app:/app \
	$(IMAGE_NODE) \
	npm run build

start: development
	@docker run --rm -it \
	-v $(PWD)/app:/app \
	-w /app \
	-p 3000:3000 \
	--entrypoint "" \
	$(IMAGE_NODE) \
	npm run start

development: build-image install
	@echo "Success!!"

serve:
	@docker run --rm -it \
	-v $(PWD)/app:/app \
	-w /app \
	-p 3000:8080 \
	--entrypoint "" \
	$(IMAGE_NODE) \
	npm run serve

ssh:
	@docker run --rm -it \
	-v $(PWD)/app:/app \
	-w /app \
	--entrypoint "" \
	$(IMAGE_NODE) \
	bash
